window.onscroll = function() {myFunction()};
var header = document.getElementById("menu_area");
var sticky = header.offsetTop;
function myFunction() 
{
  if (window.pageYOffset > sticky) 
  {
    header.classList.add("sticky");
  } 
  else 
  {
  	header.classList.add("before-sticky");
  	header.classList.remove("sticky");
  }
}


$('#reviews-slider').owlCarousel({
	margin: 20,
    loop:true,
    nav:true,
    dots:false,
    autoplay:false,
    smartSpeed:2000,
    responsive:{
        0: {items: 1},
        600: {items: 2},
        1000: {items: 3}
    }
})


$('#rasel-ahmed-main-script').owlCarousel({
  margin: 20,
    loop:true,
    nav:true,
    dots:false,
    autoplay:false,
    smartSpeed:2000,
    responsive:{
        0: {items: 1},
        600: {items: 2},
        1000: {items: 3}
    }
})